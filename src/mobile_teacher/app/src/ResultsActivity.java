package mobile_teacher.app.src;

import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import mobile_teacher.app.R;
import mobile_teacher.app.helper.DrawUIHelper;
import mobile_teacher.app.helper.ResultsHelper;
import mobile_teacher.app.helper.TypeFaceSetter;

/**
 * Created by blavi on 2/26/14.
 */
public class ResultsActivity extends BarsActivity {

    private RelativeLayout progressLayout, accuracyLayout, learningPointsLayout, resultsFooterLayout, learningPercentageLayout, testingPercentageLayout;
    private RelativeLayout resultsView;
    private TextView percentageTextView, percentageCompleted, accuracyPercentage, learningPointsValue, answeredCompletely, pointsTextView;
    private TextView accuracyLabel, progressLabel, learningPointsLabel;
    private DrawUIHelper drawUIHelper;
    private ImageView learnProgressImage, testProgressImage, accuracyImage, learningPointsImage;
    private boolean learnMode;
    private int lessonId;
    private float learningPercentage, testingPercentage;
    private Button continueLessonButton, goToDashboard;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        drawUIHelper = DrawUIHelper.getInstance(this);

        Intent i  = getIntent();
        learnMode = i.getBooleanExtra("learnMode", true);
        lessonId = Integer.parseInt(i.getStringExtra("lessonId"));
        learningPercentage = i.getFloatExtra("learningProgress", 0);
        testingPercentage = i.getFloatExtra("testingProgress", 0);

        setLayout();

    }

    public void onStart(){
        super.onStart();
    }

    protected void setLayout(){
        super.setLayout();

        resultsView = (RelativeLayout) getLayoutInflater().inflate(R.layout.results_view, null);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width, 5 * height / 7);
        container.removeAllViews();
        container.addView(resultsView);

        RelativeLayout.LayoutParams resultsLayoutParams = new RelativeLayout.LayoutParams(width - 30, 5 * height / 7);
        resultsView.setLayoutParams(resultsLayoutParams);
        container.setLayoutParams(params);
        container.setOnTouchListener(null);
        setHeader(R.layout.results_header);
        setFooter(R.layout.results_footer);

        resultsFooterLayout = (RelativeLayout) getLayoutInflater().inflate(R.layout.results_footer, null);
        continueLessonButton = (Button) findViewById(R.id.continueButton);
        goToDashboard = (Button) findViewById(R.id.goToDashboardButton);
        TypeFaceSetter.getInstance(this).setFontType(continueLessonButton);
        TypeFaceSetter.getInstance(this).setFontType(goToDashboard);

        if (learnMode) {
            continueLessonButton.setText(R.string.continueLearning);
        } else {
            continueLessonButton.setText(R.string.continueTesting);
        }

        accuracyLabel = (TextView) findViewById(R.id.accuracyLabel);
        TypeFaceSetter.getInstance(this).setFontType(accuracyLabel);
        progressLabel = (TextView) findViewById(R.id.progressLabel);
        TypeFaceSetter.getInstance(this).setFontType(progressLabel);
        learningPointsLabel = (TextView) findViewById(R.id.learningPointsLabel);
        TypeFaceSetter.getInstance(this).setFontType(learningPointsLabel);

        setProgressLayout();
        setAccuracyLayout();
        setLearningPointsLayout();
    }

    public void setLearnProgressLayout() {
        learningPercentageLayout.setVisibility(View.VISIBLE);
        testingPercentageLayout.setVisibility(View.INVISIBLE);
        percentageTextView = (TextView) findViewById(R.id.percentageTextView);
        percentageCompleted = (TextView) findViewById(R.id.percentageCompleted);
        percentageTextView.setText(String.valueOf(Math.round(learningPercentage * 100)) + "%");
        TypeFaceSetter.getInstance(this).setFontType(percentageTextView);
        percentageTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, progressLayout.getLayoutParams().width * 20 / 100);
        TypeFaceSetter.getInstance(this).setFontType(percentageCompleted);
        percentageCompleted.setTextSize(TypedValue.COMPLEX_UNIT_PX, progressLayout.getLayoutParams().width * 8 / 100);

        // Draw learn percentage image
        learnProgressImage = (ImageView) findViewById(R.id.learnProgressImageView);
        learnProgressImage.setImageBitmap(drawUIHelper.drawLearnCircle(progressLayout.getLayoutParams().width - 40, progressLayout.getLayoutParams().width - 40, learningPercentage * 100));
        Animation bounce = AnimationUtils.loadAnimation(this, R.anim.bounce);
        learnProgressImage.setAnimation(bounce);
        percentageCompleted.setAnimation(bounce);
        percentageTextView.setAnimation(bounce);
    }

    public void setTestProgressLayout() {
        learningPercentageLayout.setVisibility(View.INVISIBLE);
        testingPercentageLayout.setVisibility(View.VISIBLE);
        // Set test percentage image
        int testingPercentageImageId;
        testProgressImage = (ImageView) findViewById(R.id.testProgressImageView);

        if (testingPercentage == 1.0f)
            testingPercentageImageId = R.drawable.stars_3;
        else if (testingPercentage > 0.75f)
            testingPercentageImageId = R.drawable.stars_2;
        else if (testingPercentage > 0.50f)
            testingPercentageImageId = R.drawable.stars_1;
        else
            testingPercentageImageId = R.drawable.stars_0;

        testProgressImage.setAdjustViewBounds(true);

        if (testingPercentageImageId != -1)
            testProgressImage.setImageBitmap(drawUIHelper.drawTestCircle(progressLayout.getLayoutParams().width - 50, progressLayout.getLayoutParams().width - 50, testingPercentageImageId, testingPercentage * 100));

        Animation bounce = AnimationUtils.loadAnimation(this, R.anim.bounce);
        testProgressImage.setAnimation(bounce);

    }

    public void setProgressLayout() {
        progressLayout = (RelativeLayout) findViewById(R.id.progressLayout);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width / 3 - 50, height - 50);
        progressLayout.setLayoutParams(params);
        learningPercentageLayout = (RelativeLayout) findViewById(R.id.learnPercentageStart);
        testingPercentageLayout = (RelativeLayout) findViewById(R.id.testPercentageStart);

        if (learnMode) {
            setLearnProgressLayout();
        } else {
            setTestProgressLayout();
        }
    }

    public void setAccuracyLayout(){
        accuracyLayout = (RelativeLayout) findViewById(R.id.accuracyLayout);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width / 3 - 50, height - 50);
        accuracyLayout.setLayoutParams(params);
        accuracyImage = (ImageView) findViewById(R.id.accuracyImageView);
        accuracyPercentage = (TextView) findViewById(R.id.accuracyPercentageTextView);
        accuracyPercentage.setTextSize(TypedValue.COMPLEX_UNIT_PX, accuracyLayout.getLayoutParams().width * 20 / 100);
        answeredCompletely = (TextView) findViewById(R.id.answeredCompletely);
        answeredCompletely.setTextSize(TypedValue.COMPLEX_UNIT_PX, accuracyLayout.getLayoutParams().width * 8 / 100);
        TypeFaceSetter.getInstance(this).setFontType(answeredCompletely);
        TypeFaceSetter.getInstance(this).setFontType(accuracyPercentage);
        if (learnMode)
            accuracyPercentage.setText(String.valueOf(Math.round(ResultsHelper.calculateLearnAccuracy() * 100)) + "%");
        else
            accuracyPercentage.setText(String.valueOf(Math.round(ResultsHelper.calculateTestAccuracy() * 100)) + "%");

        accuracyImage.setImageBitmap(drawUIHelper.drawAccuracyCircle(accuracyLayout.getLayoutParams().width - 40, accuracyLayout.getLayoutParams().width - 40));
        Animation bounce = AnimationUtils.loadAnimation(this, R.anim.bounce);
        accuracyImage.setAnimation(bounce);
        accuracyPercentage.setAnimation(bounce);
        answeredCompletely.setAnimation(bounce);

    }

    public void setLearningPointsLayout(){
        learningPointsLayout = (RelativeLayout) findViewById(R.id.learningPointsLayout);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width / 3 - 50, height - 50);
        learningPointsLayout.setLayoutParams(params);
        learningPointsImage = (ImageView) findViewById(R.id.learningPointsImageView);
        learningPointsValue = (TextView) findViewById(R.id.learningPointsValue);
        learningPointsValue.setTextSize(TypedValue.COMPLEX_UNIT_PX, learningPointsLayout.getLayoutParams().width * 20 / 100);
        pointsTextView = (TextView) findViewById(R.id.points);
        pointsTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, learningPointsLayout.getLayoutParams().width * 8 / 100);
        TypeFaceSetter.getInstance(this).setFontType(pointsTextView);
        TypeFaceSetter.getInstance(this).setFontType(learningPointsValue);
        if (learnMode)
            learningPointsValue.setText(String.valueOf(Math.round(ResultsHelper.calculateLearningPoints())));
        else
            learningPointsValue.setText(String.valueOf(Math.round(ResultsHelper.calculateTestingPoints())));
        Animation bounce = AnimationUtils.loadAnimation(this, R.anim.bounce);
        learningPointsImage.setAnimation(bounce);
        learningPointsImage.setImageBitmap(drawUIHelper.drawLearningPointsCircle(learningPointsLayout.getLayoutParams().width - 40, learningPointsLayout.getLayoutParams().width - 40));
        learningPointsValue.setAnimation(bounce);
        pointsTextView.setAnimation(bounce);
    }

    public void continueLesson(View v){
        Intent i = new Intent(ResultsActivity.this, LessonActivity.class);
        i.putExtra("learn_mode", learnMode);
        i.putExtra("id", String.valueOf(lessonId));
        ResultsActivity.this.startActivity(i);
    }

    public void goToDashboard(View v) {
        Intent i = new Intent(ResultsActivity.this, DashboardActivity.class);
        ResultsActivity.this.startActivity(i);
    }

    @Override
    public void onBackPressed() {
        // Disable back button: don't do nothing when back button is pressed
    }
}
