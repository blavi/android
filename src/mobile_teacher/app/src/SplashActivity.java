package mobile_teacher.app.src;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;

import mobile_teacher.app.R;

import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by blavi on 3/4/14.
 */
public class SplashActivity extends Activity {

    private static final long DELAY = 500;
    private boolean scheduled = false;
    private Timer splashTimer;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.splash);


        splashTimer = new Timer();
        splashTimer.schedule(new TimerTask()
        {
            @Override
            public void run()
            {
                SplashActivity.this.finish();
                startActivity(new Intent(SplashActivity.this, DashboardActivity.class));
            }
        }, DELAY);
        scheduled = true;
    }

    public void onStart(){
        super.onStart();
        Resources appR = this.getResources();
        CharSequence appName = appR.getText(appR.getIdentifier("app_name",
                "string", this.getPackageName()));
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        if (scheduled)
            if (splashTimer != null)
            splashTimer.cancel();
        if (splashTimer != null)
            splashTimer.purge();
    }

}
