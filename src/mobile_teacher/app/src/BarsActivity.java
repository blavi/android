package mobile_teacher.app.src;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import mobile_teacher.app.R;

/**
 * Created by blavi on 2/13/14.
 */
public class BarsActivity extends Activity {

    protected LinearLayout cardViewFooter, cardViewHeader, cardView;
    protected RelativeLayout container;
    protected int width, height;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void setLayout(){
        setContentView(R.layout.card_view);

        cardView = (LinearLayout) findViewById(R.id.cardViewLayout);
        container = (RelativeLayout) getLayoutInflater().inflate(R.layout.container, null);
        cardViewHeader = (LinearLayout) getLayoutInflater().inflate(R.layout.card_view_header, null);
        cardViewFooter = (LinearLayout) getLayoutInflater().inflate(R.layout.card_view_footer, null);
        height = ((WindowManager) this.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getHeight();
        width = ((WindowManager) this.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getWidth();

//        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, height / 7 + AdSize.SMART_BANNER.getHeightInPixels(this));
//        cardViewFooter.setLayoutParams(params);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, height / 7);
        cardViewHeader.setLayoutParams(params);
        cardView.addView(cardViewHeader);
        cardView.addView(container);
        cardView.addView(cardViewFooter);


    }

    public void setHeader(int layoutId) {
        View view = getLayoutInflater().inflate(layoutId, null);

        cardViewHeader.removeAllViews();
        cardViewHeader.addView(view);
    }

    public void setFooter(int layoutId) {
        View view = getLayoutInflater().inflate(layoutId, null);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, height / 7);
        view.setLayoutParams(params);
        cardViewFooter.removeAllViews();
        cardViewFooter.addView(view);
        cardViewFooter.setLayoutParams(new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, height / 7));
    }
}
