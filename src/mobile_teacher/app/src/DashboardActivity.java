package mobile_teacher.app.src;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import mobile_teacher.app.R;
import mobile_teacher.app.helper.DatabaseHelper;
import mobile_teacher.app.helper.JSONParser;
import mobile_teacher.app.helper.LessonAdapter;
import mobile_teacher.app.model.Lesson;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by blavi on 12/17/13.
 */
public class DashboardActivity extends HeaderActivity {

    private DatabaseHelper db;
    private ListView lessonsListView;
    private JSONParser parser;
    private RelativeLayout dashboardLayout;
    private LinearLayout headerLayout;
    private ArrayList<Lesson> lessons;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        headerLayout = (LinearLayout) findViewById(R.id.headerLayout);
        dashboardLayout = (RelativeLayout)getLayoutInflater().inflate(R.layout.dashboard, null);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.BELOW, headerLayout.getId());
        dashboardLayout.setLayoutParams(params);
        headerLayout.addView(dashboardLayout);

        lessonsListView = (ListView) findViewById(R.id.lessonsList);
        db = DatabaseHelper.getInstance(this);

        parser = new JSONParser();

        lessons = (ArrayList) db.getLessons();
        try {
            if (lessons.size() == 0) {
                lessons = parser.getLessons(getAssets().open("lessons.json"));
                db.storeLessons(lessons);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        lessonsListView.setAdapter(new LessonAdapter(this, (ArrayList)getLessonsInAlphabeticalOrder(lessons)));

        final Intent i = new Intent(DashboardActivity.this, StartLessonActivity.class);
        lessonsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Lesson lesson = ((Lesson)parent.getItemAtPosition(position));
                i.putExtra("title", lesson.getTitle());
                i.putExtra("testingPercentage", String.valueOf(lesson.getTestingProgress()));
                i.putExtra("learningPercentage", String.valueOf(lesson.getLearningProgress()));
                i.putExtra("id", String.valueOf(lesson.getId()));

                DashboardActivity.this.startActivity(i);
            }
        });
    }

    public void onStart(){
        super.onStart();
    }

    public List<Lesson> getLessonsInAlphabeticalOrder(List<Lesson> lessons) {
        Collections.sort(lessons, new CompletedStateComparator());

        return lessons;
    }

    public class CompletedStateComparator implements Comparator<Lesson> {
        @Override
        public int compare(Lesson l1, Lesson l2) {
            return l1.getTitle().compareTo(l2.getTitle());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
