package mobile_teacher.app.src;

import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.text.Html;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ScrollView;

import mobile_teacher.app.R;
import mobile_teacher.app.helper.DatabaseHelper;
import mobile_teacher.app.helper.FlipAnimation;
import mobile_teacher.app.helper.JSONParser;
import mobile_teacher.app.helper.MultipleChoiceAnswersAdapter;
import mobile_teacher.app.helper.MyTagHandler;
import mobile_teacher.app.helper.ResultsHelper;
import mobile_teacher.app.helper.TypeFaceSetter;
import mobile_teacher.app.model.AnswerMultipleChoice;
import mobile_teacher.app.model.Asset;
import mobile_teacher.app.model.Lesson;
import mobile_teacher.app.model.Question;
import mobile_teacher.app.model.QuestionMultipleChoice;
import mobile_teacher.app.model.QuestionOpen;
import mobile_teacher.app.src.BarsActivity;
import mobile_teacher.app.src.DashboardActivity;
import mobile_teacher.app.src.ResultsActivity;

/**
 * Created by blavi on 1/20/14.
 */
public class LessonActivity extends BarsActivity {

    private List<Question> allQuestions;
    private List<Question> questions;
    private DatabaseHelper db;
    private GestureDetector gestureDetector;
    private ClickListener clickListener;
    private View.OnTouchListener gestureListener;
    private static final int SWIPE_MIN_DISTANCE = 50;
    private static final int SWIPE_MAX_OFF_PATH = 200;
    private static final int SWIPE_THRESHOLD_VELOCITY = 100;
    private View questionLayout, singleAnswerLayout, multipleAnswersLayout, answerLayout;
    private TextView questionText, answerText, footerMessage;
    private int questionNb = 0;
    private ListView multipleChoiceAnswerList;
    private HorizontalScrollView hScrollView;
    private ScrollView scrollView;
    private boolean singleChoice;
    private LinearLayout questionImages, answerImages;
    private boolean zoom;
    private ProgressBar progressBar;
    private Button stopButton, checkQuestionButton, nextQuestionButton, finishLessonButton, rightButton, wrongButton;
    private ArrayList<MultipleChoiceAnswersAdapter.MultipleChoiceAnswerView> selectedAnswers;
    private String lessonId;
    private Lesson lesson;
    /**
     *  true - learn mode
     *  false - test mode
     */
    private boolean learnMode;
    private JSONParser parser;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setLayout();
        parser = new JSONParser();
        Intent i = getIntent();
        lessonId = i.getStringExtra("id");
        learnMode = i.getBooleanExtra("learn_mode", true);
        db = DatabaseHelper.getInstance(this);
        lesson = db.getLesson(lessonId);

        if (!db.checkIfLessonContentIsStored(lessonId)) {
            try {
                allQuestions = parser.getQuestions(getAssets().open("lesson_" + lessonId + ".json"));
                db.storeQuestions(allQuestions, Integer.parseInt(lessonId));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            allQuestions = db.getQuestions(lessonId);
        }

        allQuestions = (List<Question>) shuffle(allQuestions);

        if (learnMode) {
            if (isFreshLesson()) {
                // question has not been covered or it has been covered but not one question is correct
                questions = allQuestions.subList(0, 6);
            } else {
                // lesson has already been covered but not all the questions had correct answers
                questions = getLowestCompletedQuestions(allQuestions).subList(0, 6);
            }
        } else {
            questions = allQuestions;
        }

        prepareCardView(questions.get(0));

        gestureDetector = new GestureDetector(this, new SwipeGestureDetector());
        gestureListener = new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetector.onTouchEvent(event);
            }
        };

        container.setOnTouchListener(gestureListener);

        clickListener = new ClickListener();
        multipleChoiceAnswerList.setOnTouchListener(gestureListener);
        multipleChoiceAnswerList.setOnItemClickListener(clickListener);


    }

    public void onStart(){
        super.onStart();
    }

    public List<Question> getLowestCompletedQuestions(List<Question> questions) {
        Collections.sort(questions, new CompletedStateComparator());

        return questions;
    }

    public class CompletedStateComparator implements Comparator<Question> {
        @Override
        public int compare(Question q1, Question q2) {
            return Integer.valueOf(q1.getCompletedState()).compareTo(Integer.valueOf(q2.getCompletedState()));
        }
    }

    /**
     * check if this lesson has not one correct answer to a question
     * @return
     */
    public boolean isFreshLesson() {
        for (int i = 0; i < allQuestions.size(); i++) {
            if (allQuestions.get(i).getCompletedState() > 0)
                return false;
        }

        return true;
    }

    /**
     * shuffle the given array
     *
     * @param items
     * @return shuffled list of questions
     */
    public List<?> shuffle(List<?> items) {
        Collections.shuffle(items);

        return items;
    }

    protected void setLayout() {
        super.setLayout();

        questionLayout = getLayoutInflater().inflate(R.layout.question, null);
        singleAnswerLayout = getLayoutInflater().inflate(R.layout.answer, null);
        multipleAnswersLayout = getLayoutInflater().inflate(R.layout.multiple_choice_answers_list, null);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(width - 30, 5 * height / 7);

        params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        questionLayout.setLayoutParams(params);
        singleAnswerLayout.setLayoutParams(params);
        multipleAnswersLayout.setLayoutParams(params);
        container.addView(questionLayout);
        container.addView(singleAnswerLayout);
        container.addView(multipleAnswersLayout);

        setHeader(R.layout.lesson_header);

        rightButton = (Button) findViewById(R.id.rightButton);
        TypeFaceSetter.getInstance(this).setFontType(rightButton);
        wrongButton = (Button) findViewById(R.id.wrongButton);
        TypeFaceSetter.getInstance(this).setFontType(wrongButton);
        stopButton = (Button) findViewById(R.id.stopButton);
        TypeFaceSetter.getInstance(this).setFontType(stopButton);
        checkQuestionButton = (Button) findViewById(R.id.checkButon);
        TypeFaceSetter.getInstance(this).setFontType(checkQuestionButton);
        footerMessage = (TextView) findViewById(R.id.textMessage);
        TypeFaceSetter.getInstance(this).setFontType(footerMessage);

        questionText = (TextView) findViewById(R.id.questionText);
        answerText = (TextView) findViewById(R.id.answerText);

        multipleChoiceAnswerList = (ListView) findViewById(R.id.multipleChoiceAnswers);

        selectedAnswers = new ArrayList<MultipleChoiceAnswersAdapter.MultipleChoiceAnswerView>();

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
    }

    public class ClickListener implements AdapterView.OnItemClickListener {
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            ListView list = (ListView) parent;
            MultipleChoiceAnswersAdapter.MultipleChoiceAnswerView rowView;
            CheckedTextView answerButton = ((MultipleChoiceAnswersAdapter.MultipleChoiceAnswerView) view).getAnswerButton();

            if (singleChoice) {
                for (int i = 0; i < list.getChildCount(); i++) {
                    rowView = (MultipleChoiceAnswersAdapter.MultipleChoiceAnswerView) list.getChildAt(i);
                    if (i != list.getCheckedItemPosition())
                        rowView.getAnswerButton().setChecked(false);
                    rowView.setCheckedStatus(false);
                }

                setCheckedStatus(answerButton, (MultipleChoiceAnswersAdapter.MultipleChoiceAnswerView)  view);
            } else {
                setCheckedStatus(answerButton, (MultipleChoiceAnswersAdapter.MultipleChoiceAnswerView)  view);
            }
        }
    }

    public void setCheckedStatus(CheckedTextView answerButton, MultipleChoiceAnswersAdapter.MultipleChoiceAnswerView view) {
        if (!answerButton.isChecked()) {
            answerButton.setChecked(true);
            view.setCheckedStatus(true);
        }
        else {
            answerButton.setChecked(false);
            view.setCheckedStatus(false);
        }
    }

    public void stopLesson(View v) {
        Intent i = new Intent(LessonActivity.this, DashboardActivity.class);
        LessonActivity.this.startActivity(i);
    }

    private void prepareCardView(Question question){
        prepareQuestionView(question.getText(), question.getAssets());
        prepareAnswerView(question);
    }

    private void prepareQuestionView(String question, ArrayList<Asset> images) {
        container.setOnTouchListener(gestureListener);
        questionImages = (LinearLayout) findViewById(R.id.questionImagesLayout);
        setFooter(R.layout.see_answer_footer);

        setView(questionImages, questionLayout, questionText, question, images);
    }

    private void prepareAnswerView(Question question) {
        if (question instanceof QuestionOpen)
            setAnswerView(((QuestionOpen) question).getCorrectAnswer(), ((QuestionOpen) question).getAnswerImages());
        else
            setAnswerView(((QuestionMultipleChoice) question).getAnswers());
    }

    private void setView(final LinearLayout imagesLayout, final View parentLayout, final TextView textView, String answer, ArrayList<Asset> images){
        textView.setText(Html.fromHtml(answer, null, new MyTagHandler()));
        TypeFaceSetter.getInstance(this).setFontType(textView);
        imagesLayout.removeAllViewsInLayout();

        if (images != null && images.size() != 0) {
            zoom = false;
            textView.setPadding(0, 80, 0, 0);
            AssetManager assetManager = getAssets();
            InputStream is = null;

            for (int i = 0; i < images.size(); i++) {
                final ImageView image = new ImageView(this);
                try {
                    is = assetManager.open(String.valueOf(images.get(i).getId()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                image.setImageBitmap(BitmapFactory.decodeStream(is));
                image.setLayoutParams(new LinearLayout.LayoutParams(height / 4, height / 4));

                final Animation zoom_in = AnimationUtils.loadAnimation(this, R.anim.zoom_in);
                final Animation zoom_out = AnimationUtils.loadAnimation(this, R.anim.zoom_out);

                image.setOnTouchListener(new View.OnTouchListener() {

                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                            if (zoom == false) {
                                image.startAnimation(zoom_in);
                                zoom = true;
                            } else {
                                image.startAnimation(zoom_out);
                                zoom = false;
                            }
                        }
                        return true;
                    }
                });

                // control touch listener for zoom in, zoom out
                // in order for swipe functionality triggered
                // also at touch to work
                zoom_in.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        parentLayout.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View view, MotionEvent motionEvent) {
                                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                                    if (zoom == true) {
                                        image.startAnimation(zoom_out);
                                        zoom = false;
                                    }
                                }
                                return true;
                            }
                        });

                        scrollView.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View view, MotionEvent motionEvent) {
                                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                                    if (zoom == true) {
                                        image.startAnimation(zoom_out);
                                        zoom = false;
                                    }
                                }
                                return true;
                            }
                        });
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

                zoom_out.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        parentLayout.setOnTouchListener(gestureListener);
                        scrollView.setOnTouchListener(null);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

                imagesLayout.addView(image);
            }
        } else {
            ImageView transparentImage = new ImageView(this);
            transparentImage.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.transparent));
            imagesLayout.addView(transparentImage);

            textView.setPadding(0, 0, 0, 0);
        }
    }

    private void setAnswerView(String answer, ArrayList<Asset> images) {
        answerImages = (LinearLayout) findViewById(R.id.answerImagesLayout);
        answerLayout = singleAnswerLayout;

        answerText.setMaxWidth(width - 60);
        hScrollView = (HorizontalScrollView) findViewById(R.id.hScrollView);
        scrollView = (ScrollView) findViewById(R.id.answerScrollView);
        hScrollView.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                flipCard();
                return false;
            }
        });

        setView(answerImages, singleAnswerLayout, answerText, answer, images);
    }

    private void setAnswerView(ArrayList<AnswerMultipleChoice> answers) {
        multipleChoiceAnswerList.setOnTouchListener(gestureListener);
        multipleChoiceAnswerList.setOnItemClickListener(clickListener);
        answerLayout = multipleAnswersLayout;
        if (checkAnswers(answers)){
            singleChoice = true;
        } else {
            singleChoice = false;
        }
        answers = (ArrayList<AnswerMultipleChoice>) shuffle(answers);
        multipleChoiceAnswerList.setAdapter(new MultipleChoiceAnswersAdapter(this, answers));
    }

    /** Check if there are more than one correct answer
     *  returns true if only one correct answer
     *  returns false if more correct answers
     */
    private boolean checkAnswers(ArrayList<AnswerMultipleChoice> answers) {
        int count = 0;
        for (int i = 0; i < answers.size(); i++) {
            if (answers.get(i).isCorrect())
                count++;
        }
        if (count > 1)
            return false;
        return true;
    }

    private void flipCard()
    {
        View rootLayout = container;
        View cardBack = answerLayout;
        View cardFace = questionLayout;

        FlipAnimation flipAnimation = new FlipAnimation(cardFace, cardBack);

        String lessonMode;
        lessonMode = learnMode ? "learn" : "test";

        if (cardFace.getVisibility() == View.GONE)
        {
            flipAnimation.reverse();
            setFooter(R.layout.see_answer_footer);
        }
        else {
            if (answerLayout == singleAnswerLayout){
                setFooter(R.layout.single_answer_footer);
            }
            else {
                setFooter(R.layout.multiple_answer_footer);
            }
        }
        rootLayout.startAnimation(flipAnimation);
    }

    public void nextQuestion(View v) {
        String lessonMode;
        lessonMode = learnMode ? "learn" : "test";

        setNextQuestion();
    }

    public int getProgressStatus(){
        return Math.round( (questionNb + 2.0f)/(questions.size() + 1) * 100.0f );
    }

    public void setNextQuestion() {

        if (scrollView != null)
            scrollView.setOnTouchListener(null);

        updateLesson();
        saveQuestionToDatabase();

        if (questionNb != questions.size() - 1) {
            questionNb++;

            progressBar.setProgress(getProgressStatus());

            Animation animIn  = AnimationUtils.loadAnimation(this, R.anim.translate_in);
            Animation animOut = AnimationUtils.loadAnimation(this, R.anim.translate_out);
            final Question question;
            if (questionNb < questions.size()) {
                question = questions.get(questionNb);
                prepareQuestionView(question.getText(), question.getAssets());
                answerLayout.setVisibility(View.INVISIBLE);
                questionLayout.setVisibility(View.VISIBLE);
                questionLayout.startAnimation(animIn);
                answerLayout.startAnimation(animOut);
                animOut.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        prepareAnswerView(question);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }
        } else {
            goToResultsScreen();
        }
    }

    public boolean checkAnswer(String answerText, ArrayList<AnswerMultipleChoice> answers) {
        for (int j = 0; j < answers.size(); j++) {
            if (answers.get(j).getText().equals(answerText) && answers.get(j).isCorrect()) {
                return true;
            }
        }

        return false;
    }

    public void goToResultsScreen(){
        ResultsHelper.setQuestions(allQuestions, questions);
        Intent i = new Intent(LessonActivity.this, ResultsActivity.class);
        i.putExtra("learnMode", learnMode);
        i.putExtra("lessonId", lessonId);
        if (learnMode) {
            i.putExtra("learningProgress", calculateLearningProgress());
        } else {
            i.putExtra("testingProgress", calculateTestingProgress());
        }
        LessonActivity.this.startActivity(i);
    }

    public void finishLesson(View v) {
        if (learnMode && questions.get(questionNb).getLearningCorrect() == 0) {
            setNextQuestion();
        } else {
            goToResultsScreen();
        }
    }

    public void checkQuestion(View v) {
        Question currentQuestion = questions.get(questionNb);
        ArrayList<AnswerMultipleChoice> answers = ((QuestionMultipleChoice )currentQuestion).getAnswers();
        boolean correct;
        int correctAnswersNb = 0;

        for (int z = 0; z < multipleChoiceAnswerList.getCount(); z++) {
            CheckedTextView answer = ((MultipleChoiceAnswersAdapter.MultipleChoiceAnswerView) multipleChoiceAnswerList.getChildAt(z)).getAnswerButton();
            String text = (String) answer.getText();
            correct = false;
            if (checkAnswer(text, answers) ) {
                if (answer.isChecked()) {
                    correct = true;
                    correctAnswersNb++;
                } else {
                    correct = true;
                }
            }

            if (correct) {
                ((MultipleChoiceAnswersAdapter.MultipleChoiceAnswerView) multipleChoiceAnswerList.getChildAt(z)).setCorrect();
            }

            ((MultipleChoiceAnswersAdapter.MultipleChoiceAnswerView) multipleChoiceAnswerList.getChildAt(z)).refresh(text);
        }

        if (correctAnswersNb == ((QuestionMultipleChoice) currentQuestion).getCorrectAnswersNb()) {
            if (learnMode){
                currentQuestion.setLearningCorrect(1);
            } else {
                currentQuestion.setTestingCorrect(1);
            }
            setNextQuestion();
        } else {
            // if answer is wrong, disable swiping and list view checking
            container.setOnTouchListener(null);
            multipleChoiceAnswerList.setOnTouchListener(null);
            multipleChoiceAnswerList.setOnItemClickListener(null);

            if (questionNb == questions.size() - 1) {
                finishLessonButton = (Button) findViewById(R.id.finishButon);
                TypeFaceSetter.getInstance(this).setFontType(finishLessonButton);
                finishLessonButton.setVisibility(View.VISIBLE);
            } else {
                nextQuestionButton = (Button) findViewById(R.id.nextButon);
                TypeFaceSetter.getInstance(this).setFontType(nextQuestionButton);
                nextQuestionButton.setVisibility(View.VISIBLE);
            }
            v.setVisibility(View.INVISIBLE);
            if (learnMode) {
                currentQuestion.setLearningCorrect(0);
                currentQuestion.setLearningWrongAnswer(currentQuestion.getLearningWrongAnswer() + 1);
                questions.add(new QuestionMultipleChoice(currentQuestion));
            } else {
                currentQuestion.setTestingCorrect(0);
                currentQuestion.setTestingWrongAnswer(currentQuestion.getTestingWrongAnswer() + 1);
            }
        }

    }

    public void rightAnswerHandler(View v) {
        Question currentQuestion = questions.get(questionNb);

        if (learnMode) {
            currentQuestion.setLearningCorrect(1);
        } else {
            currentQuestion.setTestingCorrect(1);
        }

        setNextQuestion();
    }

    public void wrongAnswerHandler(View v) {
        Question currentQuestion = questions.get(questionNb);

        if (learnMode) {
            currentQuestion.setLearningCorrect(0);
            currentQuestion.setLearningWrongAnswer(currentQuestion.getLearningWrongAnswer() + 1);
            questions.add(new QuestionOpen(currentQuestion));
        } else {
            currentQuestion.setTestingCorrect(0);
            currentQuestion.setTestingWrongAnswer(currentQuestion.getTestingWrongAnswer() + 1);
        }

        setNextQuestion();
    }

    /**
     *  update completeState value for question and lesson progress value
     *  If a question has completedState equal to 10 and if
     *  all the questions from a session have this value as
     *  completedState then the lesson progress is 100 and the
     *  learning process is finished.
     *  If is completedState smaller than 10, the learning process
     *  is not finished yet.
     */
    public void updateLesson() {
        Question question = questions.get(questionNb);
        int completedState;
        float progress;
        if (learnMode) {
            completedState = question.getCompletedState();
            if (question.getLearningCorrect() == 1)
                if (completedState < 10)
                    question.setCompletedState(completedState + 1);
            progress = calculateLearningProgress();
            lesson.setLearningProgress(progress);
        } else {
            progress = calculateTestingProgress();
            lesson.setTestingProgress(progress);
        }
        db.updateLessonProgress(lesson);
    }

    /**
     * calculate the lesson progress
     *
     * @return sum float
     */
    public float calculateLearningProgress(){
        float sum = 0;

        for (int i = 0; i < allQuestions.size(); i++) {
            sum += allQuestions.get(i).getCompletedState();
        }

        return Math.min( sum/(allQuestions.size() * 10.f), 1.f );
    }

    /**
     * calculate testing progress
     * @return
     */
    public float calculateTestingProgress() {
        float sum = 0, progress;

        for (int i = 0; i < allQuestions.size(); i++) {
            sum += allQuestions.get(i).getTestingCorrect();
        }

        progress = sum / allQuestions.size();

        if (Float.isNaN(progress))
            return 0;
        else
            return Math.min(progress, 1.0f);
    }

    class SwipeGestureDetector extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            try {
                if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
                    return false;
                // right to left swipe
                if(e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    flipCard();
                }  else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    flipCard();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }
    }

    public void saveQuestionToDatabase(){
        DatabaseHelper db = DatabaseHelper.getInstance(this);
        db.updateQuestion(questions.get(questionNb), Integer.parseInt(lessonId));
    }

    @Override
    public void onPause() {
        super.onPause();
        saveQuestionToDatabase();
    }

    @Override
    public void onBackPressed() {
        // Disable back button: don't do nothing when back button is pressed
    }
}
