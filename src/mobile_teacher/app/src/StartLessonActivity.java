package mobile_teacher.app.src;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import mobile_teacher.app.R;
import mobile_teacher.app.helper.DrawUIHelper;
import mobile_teacher.app.helper.TypeFaceSetter;

/**
 * Created by blavi on 1/8/14.
 */
public class StartLessonActivity extends HeaderActivity {

    private TextView titleTextView, percentageTextView, percentageCompleted, learnLabel, testLabel;
    private Button startLearn, startTest;
    private ImageView learnProgressImage, testProgressImage;
    private DrawUIHelper drawUIHelper;
    private RelativeLayout learnLayout, testLayout;
    private RelativeLayout startLessonLayout;
    private LinearLayout headerLayout;
    private float learningPercentage, testingPercentage;

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        learningPercentage = Float.parseFloat(getIntent().getStringExtra("learningPercentage"));
        testingPercentage = Float.parseFloat(getIntent().getStringExtra("testingPercentage"));
        setLayout();
    }

    public void onStart(){
        super.onStart();
    }

    public void goToLessonInLearnMode(View v) {
        Intent i = prepareIntent();
        i.putExtra("learn_mode", true);
        StartLessonActivity.this.startActivity(i);
    }

    public void goToLessonInTestMode(View v) {
        Intent i = prepareIntent();
        i.putExtra("learn_mode", false);
        StartLessonActivity.this.startActivity(i);
    }

    public Intent prepareIntent(){
        Intent i = new Intent(StartLessonActivity.this, LessonActivity.class);
        i.putExtra("id", getIntent().getStringExtra("id"));

        return i;
    }

    public void setLayout() {
        Intent i = getIntent();

        headerLayout = (LinearLayout) findViewById(R.id.headerLayout);
        startLessonLayout = (RelativeLayout)getLayoutInflater().inflate(R.layout.start_lesson, null);
        RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        params1.addRule(RelativeLayout.BELOW, headerLayout.getId());
        startLessonLayout.setLayoutParams(params1);
        headerLayout.addView(startLessonLayout);

        titleTextView = (TextView) findViewById(R.id.startLessonTitle);
        percentageTextView = (TextView) findViewById(R.id.percentageTextView);
        startLearn = (Button) findViewById(R.id.startLearnButton);
        startTest = (Button) findViewById(R.id.startTestButton);
        learnLayout = (RelativeLayout) findViewById(R.id.startLearnLayout);
        testLayout = (RelativeLayout) findViewById(R.id.testLearnLayout);
        percentageCompleted = (TextView) findViewById(R.id.percentageCompleted);
        learnProgressImage = (ImageView) findViewById(R.id.learnProgressImageView);
        testProgressImage = (ImageView) findViewById(R.id.testProgressImageView);
        learnLabel = (TextView) findViewById(R.id.learnLabel);
        testLabel = (TextView) findViewById(R.id.testLabel);

        int width = ((WindowManager) this.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getWidth();
        int height = ((WindowManager) this.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getHeight();
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(47 * width / 100, 2 * height / 5 + 15);
        learnLayout.setLayoutParams(params);
        testLayout.setLayoutParams(params);

        // Set font for labels
        TypeFaceSetter.getInstance(this).setFontType(learnLabel);
        TypeFaceSetter.getInstance(this).setFontType(testLabel);
        TypeFaceSetter.getInstance(this).setFontType(startLearn);
        TypeFaceSetter.getInstance(this).setFontType(startTest);

        // Set width and height for Learn and Test button
        startLearn.setWidth(learnLayout.getLayoutParams().width - 30);
        startTest.setWidth(learnLayout.getLayoutParams().width - 30);
        startLearn.setHeight( (int) (height * 0.104) );
        startTest.setHeight( (int) (height * 0.104) );

        // Set percentage value and font
        titleTextView.setText(i.getStringExtra("title"));
        percentageTextView.setText(String.valueOf(Math.round(learningPercentage * 100)) + "%");
        TypeFaceSetter.getInstance(this).setFontType(percentageTextView);
        percentageTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, learnLayout.getLayoutParams().width * 30 / 100);
        TypeFaceSetter.getInstance(this).setFontType(percentageCompleted);
        percentageCompleted.setTextSize(TypedValue.COMPLEX_UNIT_PX, learnLayout.getLayoutParams().width * 10 / 100);

        // Draw learn percentage image
        drawUIHelper = DrawUIHelper.getInstance(this);
        learnProgressImage.setImageBitmap(drawUIHelper.drawLearnCircle(learnLayout.getLayoutParams().width - 15, learnLayout.getLayoutParams().width - 15, learningPercentage * 100));

        // Set test percentage image
        int testingPercentageImageId;

        if (testingPercentage == 1.0f)
            testingPercentageImageId = R.drawable.stars_3;
        else if (testingPercentage > 0.75f)
            testingPercentageImageId = R.drawable.stars_2;
        else if (testingPercentage > 0.50f)
            testingPercentageImageId = R.drawable.stars_1;
        else
            testingPercentageImageId = R.drawable.stars_0;

        testProgressImage.setAdjustViewBounds(true);
        if (testingPercentageImageId != -1)
            testProgressImage.setImageBitmap(drawUIHelper.drawTestCircle(learnLayout.getLayoutParams().width - 15, learnLayout.getLayoutParams().width - 15, testingPercentageImageId, testingPercentage * 100));
    }
}
