package mobile_teacher.app.src;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;

import mobile_teacher.app.R;

/**
 * Created by blavi on 1/31/14.
 */
public class HeaderActivity extends Activity{

    protected ImageView appTitleImage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.header);

        setLogo();
    }

    private void setLogo(){
        BitmapFactory.Options myOptions = new BitmapFactory.Options();
        myOptions.inDither = true;
        myOptions.inScaled = false;
        myOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;// important
        myOptions.inPurgeable = true;
        appTitleImage = (ImageView) findViewById(R.id.appTitleImage);
        appTitleImage.setImageBitmap(BitmapFactory.decodeResource(this.getResources(), R.drawable.logo, myOptions));
    }
}
