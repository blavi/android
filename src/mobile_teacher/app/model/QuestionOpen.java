package mobile_teacher.app.model;

import mobile_teacher.app.model.Question;

import java.util.ArrayList;

/**
 * Created by blavi on 1/15/14.
 */
public class QuestionOpen extends Question {

    private String correctAnswer;
    private String answerPicture;
    private ArrayList<Asset> answerImages;

    public QuestionOpen(){

    }

    public QuestionOpen(String questionCorrectAnswer, int questionId, String questionText, int questionCurrentState, int questionCompletedState, String answerPicture) {
        super(questionId,  questionCompletedState, questionCurrentState, questionText);
        this.correctAnswer = questionCorrectAnswer;
        this.answerPicture = answerPicture;
        if (!answerPicture.isEmpty()) {
            this.answerImages = new ArrayList<Asset>();
            answerImages.add(new Asset(null, questionId, answerPicture, false));
        }
    }

    public QuestionOpen(Question q) {
        super(q.getId(), q.getCompletedState(), q.getCurrentState(),  q.getText());
        this.correctAnswer = ((QuestionOpen) q).getCorrectAnswer();
        this.answerPicture = ((QuestionOpen) q).getAnswerPicture();
        this.answerImages = ((QuestionOpen) q).getAnswerImages();
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public String getAnswerPicture() {
        return answerPicture;
    }

    public void setAnswerPicture(String answerPicture) {
        this.answerPicture = answerPicture;
    }

    public ArrayList<Asset> getAnswerImages() {
        return answerImages;
    }

    public void setAnswerImages(ArrayList<Asset> answerImages) {
        this.answerImages = answerImages;
    }

}
