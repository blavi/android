package mobile_teacher.app.model;

import java.util.ArrayList;

/**
 * Created by blavi on 1/15/14.
 */
public class QuestionMultipleChoice extends Question{

    private ArrayList<AnswerMultipleChoice> answers;

    public QuestionMultipleChoice() {

    }

    public QuestionMultipleChoice(Question q) {
        super(q.getId(), q.getCompletedState(), q.getCurrentState(), q.getText(), q.getAssets());
        this.answers = ((QuestionMultipleChoice) q).getAnswers();
    }

    public QuestionMultipleChoice(ArrayList<AnswerMultipleChoice> answers, int questionId, String questionText, int questionCurrentState, int questionCompletedState) {
        super(questionId, questionCompletedState, questionCurrentState, questionText);
        this.answers = answers;
    }

    public ArrayList<AnswerMultipleChoice> getAnswers() {
        return answers;
    }

    public void setAnswers(ArrayList<AnswerMultipleChoice> answers) {
        this.answers = answers;
    }

    public int getCorrectAnswersNb() {
        int count = 0;
        for (int i = 0; i < answers.size(); i++)
            if (answers.get(i).isCorrect())
                count++;

        return count;
    }
}
