package mobile_teacher.app.model;

/**
 * Created by blavi on 1/15/14.
 */
public class Asset {

    private byte[] cachedImage;
    private int id;
    private String path;
    private boolean questionImage;

    public Asset() {

    }

    public Asset(byte[] cachedImage, int id, String path, boolean questionImage){
        this.cachedImage = cachedImage;
        this.id = id;
        this.path = path;
        this.questionImage = questionImage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public byte[] getCachedImage() {
        return cachedImage;
    }

    public void setCachedImage(byte[] cachedImage) {
        this.cachedImage = cachedImage;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isQuestionImage() {
        return questionImage;
    }

    public void setQuestionImage(boolean questionImage) {
        this.questionImage = questionImage;
    }
}
