package mobile_teacher.app.model;

/**
 * Created by blavi on 1/15/14.
 */
public class AnswerMultipleChoice {

    private boolean correct;
    private int id;
    private String text;

    public AnswerMultipleChoice() {

    }

    public AnswerMultipleChoice(boolean correct, int id, String text) {
        this.correct = correct;
        this.id = id;
        this.text = text;
    }

    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
