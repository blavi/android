package mobile_teacher.app.model;

import java.util.ArrayList;

/**
 * Created by blavi on 1/15/14.
 */
public class Question {

    private int id;
    /**
     * completedState is the number of times a user has answered correctly to this question
     * max value is 10
     */
    private int completedState;
    private int currentState;
    private int learningCorrect;
    private int testingCorrect;
    private int learningWrongAnswersNb;
    private int testingWrongAnswersNb;
    private String text;

    private ArrayList<Asset> assets;

    public Question(){

    }

    public Question(int id, int completedState, int currentState, String text) {
        this.id = id;
        this.completedState = completedState;
        this.currentState = currentState;
        this.learningCorrect = 0;
        this.testingCorrect = 0;
        this.learningWrongAnswersNb = 0;
        this.testingWrongAnswersNb = 0;
        this.text = text;
    }

    public Question(int id, int completedState, int currentState, String text, ArrayList<Asset> assets) {
        this.id = id;
        this.completedState = completedState;
        this.currentState = currentState;
        this.learningCorrect = 0;
        this.testingCorrect = 0;
        this.learningWrongAnswersNb = 0;
        this.testingWrongAnswersNb = 0;
        this.text = text;
        this.assets = assets;
    }

    public ArrayList<Asset> getAssets() {
        return assets;
    }

    public void setAssets(ArrayList<Asset> assets) {
        this.assets = assets;
    }

    public int getCompletedState() {
        return completedState;
    }

    public void setCompletedState(int completedState) {
        this.completedState = completedState;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCurrentState() {
        return currentState;
    }

    public void setCurrentState(int currentState) {
        this.currentState = currentState;
    }

    public int getLearningCorrect() {
        return learningCorrect;
    }

    public void setLearningCorrect(int learningCorrect) {
        this.learningCorrect = learningCorrect;
    }

    public int getTestingCorrect() {
        return testingCorrect;
    }

    public void setTestingCorrect(int testingCorrect) {
        this.testingCorrect = testingCorrect;
    }

    public int getLearningWrongAnswer() {
        return learningWrongAnswersNb;
    }

    public void setLearningWrongAnswer(int learningWrongAnswer) {
        this.learningWrongAnswersNb = learningWrongAnswer;
    }

    public int getTestingWrongAnswer() {
        return testingWrongAnswersNb;
    }

    public void setTestingWrongAnswer(int testingWrongAnswer) {
        this.testingWrongAnswersNb = testingWrongAnswer;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
