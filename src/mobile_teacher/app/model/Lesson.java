package mobile_teacher.app.model;

import java.util.ArrayList;

/**
 * Created by blavi on 12/17/13.
 */
public class Lesson {

    private int id, cardCount;
    private float learningProgress, testingProgress;
    private String title;
    private String userImagePath;
    private boolean userLesson, taken;
    private String userName;
    private ArrayList<Question> questions;

    public Lesson(){

    }

    public Lesson(int id, int card_count, String title, float learning_progress, float testing_progress, boolean taken, String name, String picture){
        this.id = id;
        this.title = title;
        this.cardCount = card_count;
        this.learningProgress = learning_progress;
        this.testingProgress = testing_progress;
        this.taken = taken;
        this.userName = name;
        this.userImagePath = picture;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCardCount() {
        return cardCount;
    }

    public void setCardCount(int cardCount) {
        this.cardCount = cardCount;
    }

    public float getLearningProgress() {
        return learningProgress;
    }

    public void setLearningProgress(float learningProgress) {
        this.learningProgress = learningProgress;
    }

    public float getTestingProgress() {
        return testingProgress;
    }

    public void setTestingProgress(float testingProgress) {
        this.testingProgress = testingProgress;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUserImagePath() {
        return userImagePath;
    }

    public void setUserImagePath(String userImagePath) {
        this.userImagePath = userImagePath;
    }

    public boolean isUserLesson() {
        return userLesson;
    }

    public void setUserLesson(boolean userLesson) {
        this.userLesson = userLesson;
    }

    public boolean isTaken() {
        return taken;
    }

    public void setTaken(boolean taken) {
        this.taken = taken;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
