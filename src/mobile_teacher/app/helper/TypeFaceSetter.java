package mobile_teacher.app.helper;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by blavi on 12/20/13.
 */
public class TypeFaceSetter {

    private static TypeFaceSetter instance = null;
    private Typeface tf;

    private TypeFaceSetter(Context c)
    {
        tf = Typeface.createFromAsset(c.getAssets(), "fonts/Asap-Regular.ttf");
    }

    public static TypeFaceSetter getInstance(Context c)
    {
        if (instance == null)
            instance = new TypeFaceSetter(c);
        return instance;
    }

    public void setFontType(TextView text)
    {
        if (text == null)
        {
            return;
        }

        text.setTypeface(tf);
    }

    public void setFontType(Button button)
    {
        if (button == null)
        {
            return;
        }

        button.setTypeface(tf);
    }
}
