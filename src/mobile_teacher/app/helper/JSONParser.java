package mobile_teacher.app.helper;

import android.os.StrictMode;
import android.util.Log;

import mobile_teacher.app.model.AnswerMultipleChoice;
import mobile_teacher.app.model.Asset;
import mobile_teacher.app.model.Lesson;
import mobile_teacher.app.model.Question;
import mobile_teacher.app.model.QuestionMultipleChoice;
import mobile_teacher.app.model.QuestionOpen;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;


/**
 * Created by blavi on 12/18/13.
 */
public class JSONParser {

    private static final String LESSONS_TAG = "lessons";
    private static final String CARD_COUNT_TAG = "card_count";
    private static final String ID_TAG = "id";
    private static final String LEARNING_PROGRESS_TAG = "learning_progress";
    private static final String TAKEN_TAG = "taken";
    private static final String TESTING_PROGRESS_TAG = "testing_progress";
    private static final String TITLE_TAG = "title";
    private static final String USER_TAG = "user";
    private static final String NAME_TAG = "name";
    private static final String PICTURE_TAG = "picture";
    private static final String QUESTIONS_TAG = "questions";
    private static final String TEXT_TAG = "text";
    private static final String TYPE_TAG = "type";
    private static final String CURRENT_STATE_TAG = "current_state";
    private static final String COMPLETED_STATE_TAG = "completed_state";
    private static final String ASSETS_TAG = "assets";
    private static final String CORRECT_TAG = "correct";
    private static final String CORRECT_ANSWER_TAG = "correct_answer";
    private static final String ANSWER_PICTURE_TAG = "answer_picture";
    private static final String ANSWERS_TAG = "answers";
    private static final String PATH_TAG = "path";

    public JSONParser(){
    }

    public ArrayList<Lesson> getLessons(InputStream file){
        JSONObject jsonObj = getLessonsJSONObject(file);
        JSONArray lessonsObjArray;
        JSONObject objLesson, objUser;
        String title, picture, name;
        int card_count, id;
        float learning_progress, testing_progress;
        boolean taken;
        ArrayList<Lesson> lessons = new ArrayList<Lesson>();

        try {
            lessonsObjArray = jsonObj.getJSONArray(LESSONS_TAG);

            for (int i = 0; i < lessonsObjArray.length(); i++){
                objLesson = lessonsObjArray.getJSONObject(i);
                id = objLesson.getInt(ID_TAG);
                card_count = objLesson.getInt(CARD_COUNT_TAG);
                title = objLesson.getString(TITLE_TAG);
                learning_progress = objLesson.getLong(LEARNING_PROGRESS_TAG);
                testing_progress = objLesson.getLong(TESTING_PROGRESS_TAG);
                taken  = objLesson.getBoolean(TAKEN_TAG);
                objUser = objLesson.getJSONObject(USER_TAG);
                name = objUser.getString(NAME_TAG);
                picture = objUser.getString(PICTURE_TAG);

                lessons.add(new Lesson(id, card_count, title, learning_progress, testing_progress, taken, name, picture));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return lessons;
    }

    public ArrayList<Question> getQuestions(InputStream file) {
        JSONObject jsonObj = getLessonsJSONObject(file);
        JSONArray questionsObjArray, answersObjArray, assetsObjArray;
        JSONObject objQuestion, objAsset, objAnswer;
        String questionText, questionType, answerText, questionCorrectAnswer, assetPath, answerPicture;
        int questionId, answerId, assetId;
        int questionCurrentState, questionCompletedState;
        boolean answerValue;

        ArrayList<Question> questions = new ArrayList<Question>();
        ArrayList<AnswerMultipleChoice> answers;
        ArrayList<Asset> assets;
        Question question;

        try {
            questionsObjArray = jsonObj.getJSONArray(QUESTIONS_TAG);

            for (int i = 0; i < questionsObjArray.length(); i++) {
                objQuestion = questionsObjArray.getJSONObject(i);
                questionId = objQuestion.getInt(ID_TAG);
                questionText = objQuestion.getString(TEXT_TAG);
                questionType = objQuestion.getString(TYPE_TAG);
                questionCurrentState = objQuestion.getInt(CURRENT_STATE_TAG);
                questionCompletedState = objQuestion.getInt(COMPLETED_STATE_TAG);

                // get answers
                if (questionType.equals("open")){
                    questionCorrectAnswer = objQuestion.getString(CORRECT_ANSWER_TAG);
                    answerPicture = objQuestion.getString(ANSWER_PICTURE_TAG);
                    question = new QuestionOpen(questionCorrectAnswer, questionId, questionText, questionCurrentState, questionCompletedState, answerPicture);
                } else {
                    answersObjArray = objQuestion.getJSONArray(ANSWERS_TAG);

                    answers = new ArrayList<AnswerMultipleChoice>();
                    for (int j = 0; j < answersObjArray.length(); j++){
                        objAnswer = answersObjArray.getJSONObject(j);
                        answerId = objAnswer.getInt(ID_TAG);
                        answerText = objAnswer.getString(TEXT_TAG);
                        answerValue = objAnswer.getBoolean(CORRECT_TAG);

                        answers.add(new AnswerMultipleChoice(answerValue, answerId, answerText));
                    }

                    question = new QuestionMultipleChoice(answers, questionId, questionText, questionCurrentState, questionCompletedState);
                }

                // get assets
                assetsObjArray = objQuestion.getJSONArray(ASSETS_TAG);
                assets = new ArrayList<Asset>();
                for (int k = 0; k < assetsObjArray.length(); k++){
                    objAsset = assetsObjArray.getJSONObject(k);
                    assetPath = objAsset.getString(PATH_TAG);
                    assetId = objAsset.getInt(ID_TAG);

                    assets.add(new Asset(null, assetId, assetPath, true));
                }
                question.setAssets(assets);

                questions.add(question);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return questions;

    }

    private JSONObject getLessonsJSONObject(InputStream file) {

        String jsonContent;
        JSONObject jsonObj = null;
        BufferedReader br = null;

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        try {
            br = new BufferedReader(new InputStreamReader(file));

            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                line = br.readLine();
            }

            jsonContent = sb.toString();
            jsonObj = new JSONObject(jsonContent);
        } catch (IOException e) {
            Log.e("Error", e.toString());
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return jsonObj;
    }

}
