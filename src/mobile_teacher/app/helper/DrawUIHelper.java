package mobile_teacher.app.helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.BitmapFactory;

/**
 * Created by blavi on 12/20/13.
 */
public class DrawUIHelper {

    private static DrawUIHelper instance = null;
    private Context mContext;

    private DrawUIHelper(Context context){
        mContext = context;
    }

    public static DrawUIHelper getInstance(Context context){
        if (instance == null)
            return new DrawUIHelper(context);
        return instance;
    }

    /**
     * formula for calculating the equivalent in degrees for a percentage
     * @param percentage
     * @return
     */
    public float calculateDegrees(float percentage) {
        return percentage * 3.6f;
    }

    public Bitmap drawLearnCircle(int width, int height, float learnProgress){
        Paint paintGray = new Paint();
        paintGray.setStyle(Paint.Style.STROKE);
        paintGray.setARGB(255, 192, 192, 192);
        paintGray.setStrokeWidth(6);
        paintGray.setAntiAlias(true);
        Paint paintProgress = new Paint();
        paintProgress.setStyle(Paint.Style.STROKE);
        paintProgress.setColor(Color.GREEN);
        paintProgress.setStrokeWidth(6);
        paintProgress.setAntiAlias(true);

        Paint innerLearnCirclePaint = new Paint();
        innerLearnCirclePaint.setStyle(Paint.Style.FILL);
        innerLearnCirclePaint.setARGB(255, 240, 248, 233);
        innerLearnCirclePaint.setAntiAlias(true);

        Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bmp);
        RectF rect = new RectF(canvas.getClipBounds());
        rect.left = rect.left + 20;
        rect.top = rect.top + 20;
        rect.right = rect.right - 10;
        rect.bottom = rect.bottom - 10;
        canvas.drawArc(rect, 0, 360, true, paintGray);
        canvas.drawArc(rect, 270, Math.round(calculateDegrees(learnProgress)), false, paintProgress);
        canvas.drawArc(rect, 0, 360, true, innerLearnCirclePaint);

       return bmp;
    }

    public Bitmap drawTestCircle(int width, int height, int imageId, float testProgress){
        Paint paintGray = new Paint();
        paintGray.setStyle(Paint.Style.STROKE);
        paintGray.setARGB(255, 192, 192, 192);
        paintGray.setStrokeWidth(6);
        paintGray.setAntiAlias(true);

        Paint innerTestCirclePaint = new Paint();
        innerTestCirclePaint.setStyle(Paint.Style.FILL_AND_STROKE);
        innerTestCirclePaint.setARGB(255, 254, 254, 223);
        innerTestCirclePaint.setAntiAlias(true);

        Paint paintProgress = new Paint();
        paintProgress.setStyle(Paint.Style.STROKE);
        paintProgress.setColor(Color.YELLOW);
        paintProgress.setStrokeWidth(6);
        paintProgress.setAntiAlias(true);

        BitmapFactory.Options myOptions = new BitmapFactory.Options();
        myOptions.inDither = true;
        myOptions.inScaled = false;
        myOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;// important
        myOptions.inPurgeable = true;

        Bitmap bmp = BitmapFactory.decodeResource(mContext.getResources(), imageId, myOptions);
        Bitmap starsBitmap = bmp.copy(Bitmap.Config.ARGB_8888, true);

        bmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bmp);
        RectF rect = new RectF(canvas.getClipBounds());
        rect.left = rect.left + 20;
        rect.top = rect.top + 20;
        rect.right = rect.right - 10;
        rect.bottom = rect.bottom - 10;
        canvas.drawArc(rect, 0, 360, true, paintGray);
        canvas.drawArc(rect, 0, 360, true, innerTestCirclePaint);
        canvas.drawArc(rect, 270, Math.round(calculateDegrees(testProgress)), false, paintProgress);

        int cx, cy;
        if (canvas.getDensity() > 320)  {
            cx = (width - starsBitmap.getWidth()) / 2 - 30;
            cy = (height - starsBitmap.getHeight()) / 2 - 30;
        } else {
            cx = (width - starsBitmap.getWidth()) / 2;
            cy = (height - starsBitmap.getHeight()) / 2;
        }
        canvas.drawBitmap(starsBitmap, cx, cy, null);

        return bmp;
    }

    public Bitmap drawCircle(int width, int height, Paint paint) {
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);

        Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bmp);
        RectF rect = new RectF(canvas.getClipBounds());
        rect.left = rect.left + 20;
        rect.top = rect.top + 20;
        rect.right = rect.right - 10;
        rect.bottom = rect.bottom - 10;
        canvas.drawArc(rect, 0, 360, true, paint);

        return bmp;
    }

    public Bitmap drawAccuracyCircle(int width, int height){
        Paint paint = new Paint();

        paint.setARGB(255, 253, 240, 235);

        return drawCircle(width, height, paint);
    }

    public Bitmap drawLearningPointsCircle(int width, int height) {
        Paint paint = new Paint();

        paint.setARGB(255, 231, 246, 251);

        return drawCircle(width, height, paint);
    }
}
