package mobile_teacher.app.helper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.LinearLayout;
import android.widget.ListView;

import mobile_teacher.app.R;
import mobile_teacher.app.model.AnswerMultipleChoice;

import java.util.ArrayList;

/**
 * Created by blavi on 1/28/14.
 */
public class MultipleChoiceAnswersAdapter extends BaseAdapter {

    private ArrayList<AnswerMultipleChoice> answers;
    private Context mContext;

    public MultipleChoiceAnswersAdapter(Context context, ArrayList<AnswerMultipleChoice> answers) {
        mContext = context;
        this.answers = answers;
    }

    @Override
    public int getCount() {
        return answers.size();
    }

    @Override
    public Object getItem(int position) {
        return answers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        AnswerMultipleChoice answer = (AnswerMultipleChoice) getItem(position);

        if (convertView == null)
        {
            convertView = new MultipleChoiceAnswerView(answer);
        }
        else
        {
            ((MultipleChoiceAnswerView) convertView).setAnswerView(answer);
        }

        return convertView;
    }

    public class MultipleChoiceAnswerView extends LinearLayout
    {
        private CheckedTextView answerRadioButton;
        private boolean correct;
        private boolean checked;

        public MultipleChoiceAnswerView(AnswerMultipleChoice answer) {
            super(mContext);

            int width = ((WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getWidth() - 60;
            ListView.LayoutParams params = new ListView.LayoutParams(width, LayoutParams.WRAP_CONTENT);
            addView(LayoutInflater.from(mContext).inflate(R.layout.answer_item, null), params);
            setAnswerView(answer);
            this.correct = false;
            this.checked = false;
        }

        public void refresh(String answer) {
            removeAllViews();

            int width = ((WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getWidth() - 60;
            ListView.LayoutParams params = new ListView.LayoutParams(width, LayoutParams.WRAP_CONTENT);
            if (getCorrectStatus()) {
                addView(LayoutInflater.from(mContext).inflate(R.layout.correct_answer_item, null), params);
            } else {
                if (getCheckedStatus()) {
                    addView(LayoutInflater.from(mContext).inflate(R.layout.wrong_answer_item, null), params);
                } else {
                    addView(LayoutInflater.from(mContext).inflate(R.layout.answer_item, null), params);
                }
            }
            setAnswerView(answer);
        }

        private void setAnswerView(String text) {
            answerRadioButton = (CheckedTextView) findViewById(R.id.answerItem);
            answerRadioButton.setText(text);
            TypeFaceSetter.getInstance(mContext).setFontType(answerRadioButton);
            if (checked)
                answerRadioButton.setChecked(true);
        }

        private void setAnswerView(AnswerMultipleChoice answer) {
            answerRadioButton = (CheckedTextView) findViewById(R.id.answerItem);
            TypeFaceSetter.getInstance(mContext).setFontType(answerRadioButton);
            answerRadioButton.setText(answer.getText());
        }

        public CheckedTextView getAnswerButton(){
            return answerRadioButton;
        }

        public void setCorrect() {
            this.correct = true;
        }

        public boolean getCorrectStatus(){
            return this.correct;
        }

        public boolean getCheckedStatus(){
            return this.checked;
        }

        public void setCheckedStatus(boolean checkedStatus) {
            this.checked = checkedStatus;
        }

    }
}
