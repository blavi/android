package mobile_teacher.app.helper;

import mobile_teacher.app.model.Question;

import java.util.List;

/**
 * Created by blavi on 2/28/14.
 */
public class ResultsHelper {

    private static List<Question> allQuestions;
    private static List randomQuestions;

    public static float calculateLearnAccuracy(){

        float totalNbOfAnswers = randomQuestions.size();
        float sum = 0;
        for (int i = 0; i < randomQuestions.size(); i++) {
            sum += ((Question)randomQuestions.get(i)).getLearningCorrect();
        }

        return Math.min(sum/totalNbOfAnswers, 1.f);
    }

    public static float calculateTestAccuracy(){
        float sum = 0;
        for (int i = 0; i < allQuestions.size(); i++) {
            sum += allQuestions.get(i).getTestingCorrect();
        }

        return Math.min(sum/allQuestions.size(), 1.f);
    }

    public static int level(Question question) {
        int[] levels =  {1,2,3,4,3,4,3,4,3,4};

        return levels[Math.max(0, Math.min(question.getCompletedState(), 9)) - 1];
    }

    public static float calculateLearningPoints() {
        int[] points = {15, 17, 19,21};
        int pointCount = 0;

        for (int i = 0; i < randomQuestions.size(); i++) {
            Question question = (Question)randomQuestions.get(i);
            if (question.getLearningCorrect() != 0){
                pointCount += points[Math.max(0, Math.min( level(question) , 16) ) - 1];
            }
        }

        if (cycleComplete()){
            pointCount += 300;
        }
        pointCount += 30;

        return pointCount;
    }

    public static float calculateTestingPoints() {
        float pointCount = 0;

        for (int i = 0; i < allQuestions.size(); i++) {
            if (allQuestions.get(i).getTestingCorrect() != 0) {
                pointCount += 21;
            }
        }

        float multiplier;
        float testingAccuracy = calculateTestAccuracy();

        if (testingAccuracy < 0.5f) {
            multiplier = 0.f;
        } else if (testingAccuracy < 0.75f) {
            multiplier = 0.5f;
        } else if (testingAccuracy < 1.0f) {
            multiplier = 0.75f;
        } else {
            multiplier = 1.0f;
        }

        pointCount = (1.f + multiplier) * pointCount;

        return pointCount;
    }

    public static boolean cycleComplete() {
        for (int i = 0; i < allQuestions.size(); i++) {
            if (allQuestions.get(i).getCompletedState() < 10)
                return false;
        }

        return true;
    }

    public static void setQuestions(List<Question> all_questions, List<Question> random_questions){
        allQuestions = all_questions;
        randomQuestions = random_questions;
    }
}
