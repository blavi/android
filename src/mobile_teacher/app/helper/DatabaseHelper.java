package mobile_teacher.app.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import mobile_teacher.app.model.AnswerMultipleChoice;
import mobile_teacher.app.model.Asset;
import mobile_teacher.app.model.Lesson;
import mobile_teacher.app.model.Question;
import mobile_teacher.app.model.QuestionMultipleChoice;
import mobile_teacher.app.model.QuestionOpen;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by blavi on 12/17/13.
 */
public class DatabaseHelper extends SQLiteOpenHelper{

    private static DatabaseHelper databaseHelper = null;

    // Database version
    private static final int DATABASE_VERSION = 1;

    // Database name
    private static final String DATABASE_NAME = "CRAMLR Database";

    // Table names
    private static final String LESSON_TABLE                   = "Lesson";
    private static final String QUESTION_TABLE                 = "Question";
    private static final String QUESTION_OPEN_TABLE            = "QuestionOpen";
    private static final String ASSET_TABLE                    = "Asset";
    private static final String QUESTION_MULTIPLE_CHOICE_TABLE = "QuestionMultipleChoice";
    private static final String ANSWER_MULTIPLE_CHOICE_TABLE   = "AnswerMultipleChoice";

    // Common column names
    private static final String ONLINE_ID                          = "online_id";

    // Lesson table - column names
    private static final String LESSON_CARD_COUNT           = "cardCount";
    private static final String LESSON_LEARNING_PROGRESS    = "learningProgress";
    private static final String LESSON_TESTING_PROGRESS     = "testingProgress";
    private static final String LESSON_TITLE                = "title";
    private static final String LESSON_USER_IMAGEPATH       = "userImagePath";
    private static final String LESSON_USER_LESSON          = "userLesson";
    private static final String LESSON_USER_NAME            = "userName";
    private static final String LESSON_TAKEN                = "taken";
    private static final String QUESTION_ID                 = "questionId";

    // Question table - column names
    private static final String QUESTION_COMPLETED_STATE             = "completedState";
    private static final String QUESTION_CURRENT_STATE               = "currentState";
    private static final String QUESTION_LEARNING_CORRECT            = "learningCorrect";
    private static final String QUESTION_TESTING_CORRECT             = "testingCorrect";
    private static final String QUESTION_LEARNING_WRONG_ANSWERS_NB   = "learningWrongAnswersNb";
    private static final String QUESTION_TESTING_WRONG_ANSWERS_NB    = "testingWrongAnswersNb";
    private static final String QUESTION_TEXT                        = "text";
    private static final String QUESTION_CORRECT_ANSWER              = "correctAnswer";
    private static final String QUESTION_ANSWER_PICTURE              = "answerPicture";
    private static final String ASSET_ID                             = "assetID";
    private static final String LESSON_ID                            = "lessonId";

    // Asset table - column names
    private static final String ASSET_CACHED_IMAGE  = "cachedImage";
    private static final String ASSET_PATH          = "path";
    private static final String QUESTION_IMAGE      = "questionImage";

    // QuestionOpen table - column names
    private static final String QUESTION_OPEN_CORRECT_ANSWER = "correctAnswer";

    // AnswerMultipleChoice table - column names
    private static final String ANSWER_MULTIPLE_CHOICE_CORRECT  = "correct";
    private static final String ANSWER_MULTIPLE_CHOICE_TEXT     = "text";

    // Table create statements

    // Lessons table
    private static final String CREATE_TABLE_LESSON = "CREATE TABLE " + LESSON_TABLE
            + "("
            + ONLINE_ID + " INTEGER, "
            + LESSON_CARD_COUNT + " INTEGER, "
            + LESSON_LEARNING_PROGRESS + " FLOAT, "
            + LESSON_TESTING_PROGRESS + " FLOAT, "
            + LESSON_TITLE + " TEXT, "
            + LESSON_USER_IMAGEPATH + " TEXT, "
            + LESSON_USER_LESSON + " BOOLEAN, "
            + LESSON_USER_NAME + " TEXT, "
            + LESSON_TAKEN + " BOOLEAN"
            + ");";

    // Questions table
    private static final String CREATE_TABLE_QUESTION = "CREATE TABLE " + QUESTION_TABLE
            + "("
            + ONLINE_ID + " INTEGER, "
            + QUESTION_COMPLETED_STATE + " INTEGER, "
            + QUESTION_CURRENT_STATE + " INTEGER, "
            + QUESTION_LEARNING_CORRECT + " INTEGER, "
            + QUESTION_TESTING_CORRECT + " INTEGER, "
            + QUESTION_LEARNING_WRONG_ANSWERS_NB + " INTEGER, "
            + QUESTION_TESTING_WRONG_ANSWERS_NB + " INTEGER, "
            + QUESTION_TEXT + " TEXT, "
            + QUESTION_CORRECT_ANSWER + " TEXT, "
            + QUESTION_ANSWER_PICTURE + " TEXT, "
            + LESSON_ID + " INTEGER, "
            + "FOREIGN KEY(" + LESSON_ID + ") REFERENCES " + LESSON_TABLE + "(" + ONLINE_ID + ") "

            + ");";

    // Asset table
    private static final String CREATE_TABLE_ASSET = "CREATE TABLE " + ASSET_TABLE
            + "("
            + ONLINE_ID + " INTEGER, "
            + ASSET_CACHED_IMAGE + " BLOB, "
            + ASSET_PATH + " TEXT, "
            + QUESTION_IMAGE + " BOOLEAN, "
            + QUESTION_ID + " INTEGER, "
            + "FOREIGN KEY(" + QUESTION_ID + ") REFERENCES " + QUESTION_TABLE + "(" + ONLINE_ID + ") "
            + ");";

    // AnswerMultipleChoice table
    private static final String CREATE_TABLE_ANSWER_MULTIPLE_CHOICE = "CREATE TABLE " + ANSWER_MULTIPLE_CHOICE_TABLE
            + "("
            + ONLINE_ID + " INTEGER, "
            + ANSWER_MULTIPLE_CHOICE_CORRECT + " BOOLEAN, "
            + ANSWER_MULTIPLE_CHOICE_TEXT + " TEXT, "
            + QUESTION_ID + " INTEGER, "
            + "FOREIGN KEY(" + QUESTION_ID + ") REFERENCES " + QUESTION_TABLE + "(" + ONLINE_ID + ")"
            + ");";

    private DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static DatabaseHelper getInstance(Context context)
    {
        if (databaseHelper == null)
            return new DatabaseHelper(context);
        return databaseHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // creating required tables
        db.execSQL(CREATE_TABLE_LESSON);
        db.execSQL(CREATE_TABLE_QUESTION);
        db.execSQL(CREATE_TABLE_ASSET);
        db.execSQL(CREATE_TABLE_ANSWER_MULTIPLE_CHOICE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // on upgrade drop older tables
        db.execSQL("DROP TABLE IF EXISTS " + LESSON_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + QUESTION_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + ASSET_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + ANSWER_MULTIPLE_CHOICE_TABLE);

        // create new tables
        onCreate(db);
    }

    public SQLiteDatabase getDatabase()
    {
        return getWritableDatabase();
    }

    public void storeLessons(ArrayList<Lesson> lessons){
        ContentValues values = new ContentValues();

        SQLiteDatabase db = null;
        try {
            db = getDatabase();
            for (Lesson lesson : lessons) {
                values.put(ONLINE_ID, lesson.getId());
                values.put(LESSON_CARD_COUNT, lesson.getCardCount());
                values.put(LESSON_LEARNING_PROGRESS, lesson.getLearningProgress());
                values.put(LESSON_TESTING_PROGRESS, lesson.getTestingProgress());
                values.put(LESSON_TITLE, lesson.getTitle());
                values.put(LESSON_USER_IMAGEPATH, lesson.getUserImagePath());
                values.put(LESSON_USER_LESSON, false);
                values.put(LESSON_USER_NAME, lesson.getUserName());
                values.put(LESSON_TAKEN, lesson.isTaken());

                db.insert(LESSON_TABLE, null, values);

                values.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (db != null)
                db.close();
        }
    }

    public List<Lesson> getLessons(){
        List<Lesson> lessons = new ArrayList<Lesson>();
        String selectQuery = "SELECT * FROM " + LESSON_TABLE;

        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            db = getDatabase();
            c = db.rawQuery(selectQuery, null);
            if (c.moveToFirst()){
                do {
                    Lesson lesson = new Lesson();
                    lesson.setId(c.getInt(c.getColumnIndex(ONLINE_ID)));
                    lesson.setCardCount(c.getInt(c.getColumnIndex(LESSON_CARD_COUNT)));
                    lesson.setLearningProgress(c.getFloat(c.getColumnIndex(LESSON_LEARNING_PROGRESS)));
                    lesson.setTestingProgress(c.getFloat(c.getColumnIndex(LESSON_TESTING_PROGRESS)));
                    lesson.setTitle(c.getString(c.getColumnIndex(LESSON_TITLE)));
                    lesson.setUserImagePath(c.getString(c.getColumnIndex(LESSON_USER_IMAGEPATH)));
                    lesson.setUserLesson(c.getColumnIndex(LESSON_USER_LESSON) > 0);
                    lesson.setUserName(c.getString(c.getColumnIndex(LESSON_USER_NAME)));
                    lesson.setTaken(c.getColumnIndex(LESSON_TAKEN) > 0);

                    lessons.add(lesson);
                }
                while (c.moveToNext());

            }
        } catch (Exception e) {

        } finally {
            if (c != null && db != null) {
                c.close();
                db.close();
            }
        }

        return lessons;
    }

    public List<Question> getQuestions(String lessonId) {
        List<Question> questions = new ArrayList<Question>();
        List<Asset> assets;
        List<AnswerMultipleChoice> answers;
        List<Asset> images;
        String selectQuery = "SELECT * FROM " + QUESTION_TABLE + " WHERE " + LESSON_ID + " = '"+ lessonId + "'";

        SQLiteDatabase db = null;
        Cursor c = null;
        Question question;
        String  text;

        try {
            db = getDatabase();
            c = db.rawQuery(selectQuery, null);

            if (c.moveToFirst()){
                do {
                    text = c.getString(c.getColumnIndex(QUESTION_CORRECT_ANSWER));
                    if (text != null) {
                        question = new QuestionOpen();
                        ((QuestionOpen)question).setCorrectAnswer(text);
                        ((QuestionOpen)question).setAnswerPicture(c.getString(c.getColumnIndex(QUESTION_ANSWER_PICTURE)));
                        // set answer images
                        images = getAssets(db, c.getInt(c.getColumnIndex(ONLINE_ID)), false);
                        ((QuestionOpen)question).setAnswerImages((ArrayList)images);
                    } else {
                        question = new QuestionMultipleChoice();
                        answers = getAnswers(db, c.getInt(c.getColumnIndex(ONLINE_ID)));
                        ((QuestionMultipleChoice) question).setAnswers((ArrayList<AnswerMultipleChoice>)answers);
                    }

                    question.setId(c.getInt(c.getColumnIndex(ONLINE_ID)));
                    question.setCompletedState(c.getInt(c.getColumnIndex(QUESTION_COMPLETED_STATE)));
                    question.setCurrentState(c.getInt(c.getColumnIndex(QUESTION_CURRENT_STATE)));
                    question.setLearningCorrect(c.getInt(c.getColumnIndex(QUESTION_LEARNING_CORRECT)));
                    question.setTestingCorrect(c.getInt(c.getColumnIndex(QUESTION_TESTING_CORRECT)));
                    question.setLearningWrongAnswer(c.getInt(c.getColumnIndex(QUESTION_LEARNING_WRONG_ANSWERS_NB)));
                    question.setTestingWrongAnswer(c.getInt(c.getColumnIndex(QUESTION_TESTING_WRONG_ANSWERS_NB)));
                    question.setText(c.getString(c.getColumnIndex(QUESTION_TEXT)));

                    assets = getAssets(db, c.getInt(c.getColumnIndex(ONLINE_ID)), true);
                    question.setAssets((ArrayList<Asset>) assets);

                    questions.add(question);
                }
                while (c.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (c != null && db != null) {
                c.close();
                db.close();
            }
        }

        return questions;
    }

    public List<Asset> getAssets(SQLiteDatabase db, int questionId, boolean questionImage){
        int val = questionImage ? 1 : 0;

        String selectQuery = "SELECT * FROM " + ASSET_TABLE + " WHERE " + QUESTION_ID + " = '"+ String.valueOf(questionId) + "' AND " + QUESTION_IMAGE + " = "+ val + "";

        Cursor c = null;

        List<Asset> assets = new ArrayList<Asset>();
        Asset asset;
        try {
            c = db.rawQuery(selectQuery, null);
            if (c.moveToFirst()){
                do {
                    asset = new Asset();
                    asset.setId(c.getInt(c.getColumnIndex(ONLINE_ID)));
                    asset.setCachedImage(c.getBlob(c.getColumnIndex(ASSET_CACHED_IMAGE)));
                    asset.setPath(c.getString(c.getColumnIndex(ASSET_PATH)));
                    asset.setQuestionImage(c.getInt(c.getColumnIndex(QUESTION_IMAGE)) > 0);
                    assets.add(asset);
                }
                while(c.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (c != null) {
                c.close();
            }
        }

        return assets;
    }

    public List<AnswerMultipleChoice> getAnswers(SQLiteDatabase db, int questionId) {
        String selectQuery = "SELECT * FROM " + ANSWER_MULTIPLE_CHOICE_TABLE + " WHERE " + QUESTION_ID + " = '"+ String.valueOf(questionId) + "'";

        Cursor c = null;

        List<AnswerMultipleChoice> answers = new ArrayList<AnswerMultipleChoice>();
        AnswerMultipleChoice answer;
        try {
            c = db.rawQuery(selectQuery, null);
            if (c.moveToFirst()){
                do {
                    answer = new AnswerMultipleChoice();

                    answer.setId(c.getInt(c.getColumnIndex(ONLINE_ID)));
                    answer.setCorrect(c.getInt(c.getColumnIndex(ANSWER_MULTIPLE_CHOICE_CORRECT)) > 0);
                    answer.setText(c.getString(c.getColumnIndex(ANSWER_MULTIPLE_CHOICE_TEXT)));

                    answers.add(answer);
                }
                while(c.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (c != null)
                c.close();
        }

        return answers;
    }

    public void storeQuestions(List<Question> questions, int lessonId) {
        ContentValues values = new ContentValues();
        String answerPicture;
        SQLiteDatabase db = null;

        try {
            db = getDatabase();
            for (Question question : questions) {
                values.put(ONLINE_ID, question.getId());
                values.put(QUESTION_COMPLETED_STATE, question.getCompletedState());
                values.put(QUESTION_CURRENT_STATE, question.getCurrentState());
                values.put(QUESTION_LEARNING_CORRECT, question.getLearningCorrect());
                values.put(QUESTION_TESTING_CORRECT, question.getTestingCorrect());
                values.put(QUESTION_LEARNING_WRONG_ANSWERS_NB, question.getLearningWrongAnswer());
                values.put(QUESTION_TESTING_WRONG_ANSWERS_NB, question.getTestingWrongAnswer());
                values.put(QUESTION_TEXT, question.getText());
                values.put(LESSON_ID, lessonId);

                if (question instanceof QuestionOpen) {
                    values.put(QUESTION_CORRECT_ANSWER, ((QuestionOpen) question).getCorrectAnswer());
                    answerPicture = ((QuestionOpen) question).getAnswerPicture();
                    values.put(QUESTION_ANSWER_PICTURE, answerPicture);
                    if (!answerPicture.isEmpty())
                        storeAssets(db, ((QuestionOpen) question).getAnswerImages(), question.getId());
                }
                if (question instanceof QuestionMultipleChoice)
                    storeMultipleChoiceAnswers(db, ((QuestionMultipleChoice)question).getAnswers(), question.getId());
                if (!question.getAssets().isEmpty()) {
                    storeAssets(db, question.getAssets(), question.getId());
                }

                db.insert(QUESTION_TABLE, null, values);

                values.clear();
            }
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            db.close();
        }
    }

    public void storeMultipleChoiceAnswers(SQLiteDatabase db, ArrayList<AnswerMultipleChoice> answers, int onlineID) {
        ContentValues values = new ContentValues();

        for (AnswerMultipleChoice answer : answers) {
            values.put(ONLINE_ID, answer.getId());
            values.put(ANSWER_MULTIPLE_CHOICE_TEXT, answer.getText());
            values.put(ANSWER_MULTIPLE_CHOICE_CORRECT, answer.isCorrect());
            values.put(QUESTION_ID, onlineID);

            db.insert(ANSWER_MULTIPLE_CHOICE_TABLE, null, values);

            values.clear();
        }

    }

    public void storeAssets(SQLiteDatabase db, ArrayList<Asset> assets, int onlineID) {
        ContentValues values = new ContentValues();

        for (Asset asset : assets) {
            values.put(ONLINE_ID, asset.getId());
            values.put(ASSET_PATH, asset.getPath());
            values.put(ASSET_CACHED_IMAGE, new byte[0]);
            values.put(QUESTION_IMAGE, asset.isQuestionImage());
            values.put(QUESTION_ID, onlineID);

            db.insert(ASSET_TABLE, null, values);

            values.clear();
        }

    }

    public boolean checkIfLessonContentIsStored(String lessonId) {
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            db = getDatabase();
            c = db.rawQuery("SELECT * FROM " + QUESTION_TABLE + " WHERE " + LESSON_ID + " = '" + lessonId + "'", null);
            int nr = c.getCount();
            if (nr != 0) {
                c.close();
                db.close();
                return true;
            }
        } catch(Exception e){
            e.printStackTrace();
        } finally {
            if (c != null && db != null) {
                c.close();
                db.close();
            }
        }

        return false;
    }

    public Lesson getLesson(String lessonId){
        Lesson lesson = new Lesson();
        SQLiteDatabase db = null;
        Cursor c = null;

        try {
            db = getDatabase();
            c = db.rawQuery("SELECT * FROM " + LESSON_TABLE + " WHERE " + ONLINE_ID + " = '" + lessonId + "'", null);

            if (c.moveToFirst()) {
                lesson.setId(c.getInt(c.getColumnIndex(ONLINE_ID)));
                lesson.setCardCount(c.getInt(c.getColumnIndex(LESSON_CARD_COUNT)));
                lesson.setLearningProgress(c.getFloat(c.getColumnIndex(LESSON_LEARNING_PROGRESS)));
                lesson.setTestingProgress(c.getFloat(c.getColumnIndex(LESSON_TESTING_PROGRESS)));
                lesson.setTitle(c.getString(c.getColumnIndex(LESSON_TITLE)));
                lesson.setUserImagePath(c.getString(c.getColumnIndex(LESSON_USER_IMAGEPATH)));
                lesson.setUserLesson(c.getColumnIndex(LESSON_USER_LESSON) > 0);
                lesson.setUserName(c.getString(c.getColumnIndex(LESSON_USER_NAME)));
                lesson.setTaken(c.getColumnIndex(LESSON_TAKEN) > 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (c != null && db != null) {
                c.close();
                db.close();
            }
        }

        return lesson;
    }

    public void updateLessonProgress(Lesson lesson) {
        ContentValues values = new ContentValues();

        SQLiteDatabase db = null;

        values.put(ONLINE_ID, lesson.getId());
        values.put(LESSON_CARD_COUNT, lesson.getCardCount());
        values.put(LESSON_LEARNING_PROGRESS, lesson.getLearningProgress());
        values.put(LESSON_TESTING_PROGRESS, lesson.getTestingProgress());
        values.put(LESSON_TITLE, lesson.getTitle());
        values.put(LESSON_USER_IMAGEPATH, lesson.getUserImagePath());
        values.put(LESSON_USER_LESSON, lesson.isUserLesson());
        values.put(LESSON_USER_NAME, lesson.getUserName());
        values.put(LESSON_TAKEN, lesson.isTaken());

        try {
            db = getDatabase();
            db.update(LESSON_TABLE, values, ONLINE_ID + " = '" + lesson.getId() + "'", null);
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }

    public void updateQuestion(Question question, int lessonId) {
        ContentValues values = new ContentValues();

        SQLiteDatabase db = null;

        values.put(ONLINE_ID, question.getId());
        values.put(QUESTION_COMPLETED_STATE, question.getCompletedState());
        values.put(QUESTION_CURRENT_STATE, question.getCurrentState());
        values.put(QUESTION_LEARNING_CORRECT, question.getLearningCorrect());
        values.put(QUESTION_TESTING_CORRECT, question.getTestingCorrect());
        values.put(QUESTION_LEARNING_WRONG_ANSWERS_NB, question.getLearningWrongAnswer());
        values.put(QUESTION_TESTING_WRONG_ANSWERS_NB, question.getTestingWrongAnswer());
        values.put(LESSON_ID, lessonId);

        if (question instanceof QuestionOpen)
            values.put(QUESTION_CORRECT_ANSWER, ((QuestionOpen) question).getCorrectAnswer());

        try {
            db = getDatabase();
            db.update(QUESTION_TABLE, values, ONLINE_ID + " = '" + question.getId() + "'", null);
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            if (db != null)
                db.close();
        }
    }
}
