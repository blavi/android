package mobile_teacher.app.helper;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import mobile_teacher.app.model.Lesson;
import mobile_teacher.app.R;

import java.util.ArrayList;

/**
 * Created by blavi on 12/17/13.
 */
public class LessonAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<Lesson> lessons;

    public LessonAdapter(Context context, ArrayList<Lesson> lessons) {
        mContext = context;
        this.lessons = lessons;
    }

    public int getCount()
    {
        return lessons.size();
    }

    public Object getItem(int position)
    {
        return lessons.get(position);
    }

    public long getItemId(int position)
    {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        Lesson lesson = (Lesson) getItem(position);

        if (convertView == null)
        {
            convertView = new LessonView(lesson);
        }
        else
        {
            ((LessonView) convertView).setLessonView(lesson);
        }

        return convertView;
    }

    public class LessonView extends LinearLayout
    {
        private TextView lessonTitle, cardsNr, percentageValue, percentageCompleted;
        private ImageView progressImage;
        private DrawUIHelper drawUIHelper;
        private RelativeLayout lessonRow;

        public LessonView(Lesson lesson)
        {
            super(mContext);
            drawUIHelper = DrawUIHelper.getInstance(mContext);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(((WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getWidth(), (int) (((WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getHeight() * 0.14));
            addView(LayoutInflater.from(mContext).inflate(R.layout.lesson_item, null), params);

            setLessonView(lesson);
        }

        /**
         * @param lesson
         */
        private void setLessonView(Lesson lesson)
        {
            lessonTitle = (TextView) findViewById(R.id.lessonTitleTextView);
            lessonTitle.setText(lesson.getTitle());

            TypeFaceSetter.getInstance(mContext).setFontType(lessonTitle);
            cardsNr = (TextView) findViewById(R.id.cardsNrTextView);
            cardsNr.setText(String.valueOf(lesson.getCardCount()));

            progressImage = (ImageView) findViewById(R.id.learnProgressImageView);
            lessonRow = (RelativeLayout) findViewById(R.id.lessonItem);

            lessonTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, lessonRow.getLayoutParams().width / 20);
            percentageValue = (TextView) findViewById(R.id.percentageTextView);
            TypeFaceSetter.getInstance(mContext).setFontType(percentageValue);
            percentageValue.setTextSize(TypedValue.COMPLEX_UNIT_PX, lessonRow.getLayoutParams().width / 15);
            percentageValue.setText(String.valueOf(Math.round(lesson.getLearningProgress() * 100)) + "%");
            percentageCompleted = (TextView) findViewById(R.id.percentageCompleted);
            TypeFaceSetter.getInstance(mContext).setFontType(percentageCompleted);
            percentageCompleted.setTextSize(TypedValue.COMPLEX_UNIT_PX, lessonRow.getLayoutParams().width / 50);

            progressImage.setImageBitmap(drawUIHelper.drawLearnCircle(lessonRow.getLayoutParams().height + 40, lessonRow.getLayoutParams().height + 40, lesson.getLearningProgress() * 100));
        }
    }

}
