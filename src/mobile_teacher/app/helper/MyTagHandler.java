package mobile_teacher.app.helper;

import android.text.Editable;
import android.text.Html;

import org.xml.sax.XMLReader;

/**
 * Created by blavi on 4/2/14.
 */
public class MyTagHandler implements Html.TagHandler {
    boolean first= true;

    @Override
    public void handleTag(boolean opening, String tag, Editable output,
                          XMLReader xmlReader) {

        if(tag.equals("li")){
            if(first){
                output.append("\n\t\u2022");
                first= false;
            }else{
                first = true;
            }
        }
    }
}
