package mobile_teacher.app.tests;

import android.content.Intent;
import android.test.ActivityInstrumentationTestCase2;
import mobile_teacher.app.R;
import mobile_teacher.app.src.DashboardActivity;
import junit.framework.*;

//public class DashboardTest extends TestCase {
//    public void testTrue()
//    {
//        assertTrue(true);
//    }
//}

public class DashboardTest extends ActivityInstrumentationTestCase2<DashboardActivity> {
	
	private DashboardActivity mActivity;
	
	public DashboardTest(){
    	super(DashboardActivity.class);
    }
	
	public DashboardTest(Class<DashboardActivity> activityClass) {
		super(activityClass);
		// TODO Auto-generated constructor stub
	}

	@Override
    protected void setUp() throws Exception {
    	super.setUp();
    //	mActivity = getActivity(); // get a references to the app under test
    }
    
	public void testLayout()
    {
        assertNotNull(getActivity().findViewById(R.id.lessonsList));
    }
}
//
//import android.util.Log;
//import android.test.InstrumentationTestCase;
//
//public class DashboardTest extends InstrumentationTestCase {
//    private static final String TAG = "AndroidTest";
//
//    @Override
//    protected void setUp() throws Exception {
//	Log.d(TAG, "setUp(): " + getName());
//        super.setUp();
//    }
//
//    @Override 
//    protected void tearDown() throws Exception {
//	Log.d(TAG, "tearDown(): " + getName());
//        super.tearDown();
//    }
//
//    public void testCase1() {
//	assertTrue(true);
//    }
//
//    public void testCase2() {
//	assertEquals("12", 12);
//    }
//
//    public void testCase3() throws Exception {
//	throw new Exception();
//    }
//
//    public void thisIsNotATest() {
//	Log.d(TAG, "This is not a test");
//    }
//}